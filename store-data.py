import opensmile as opensmile
import os.path
from tqdm import tqdm

def main():
    PTSS_interview_ids = ["645","896","1713","1714"]
    PTSS_interview_ids.extend(["316","338","683"])

    source_dir = r"E:\thesis_audio\split-audio-2"
    target_dir = r"E:/thesis_audio/data-2/raw_data/"

    for filename in tqdm(os.listdir(source_dir), colour='blue'):
        data_file_name = "{}.json".format(filename.split('-')[0])
        file_path = os.path.join(source_dir, filename)

        #true if PTSS else false
        PTSS_label = any(filename.startswith(iid) for iid in PTSS_interview_ids)

        #get smile data frame of fragment
        features_df = parse_features_to_df(file_path, PTSS_label)

        #store raw data in json-file
        save_raw_data([features_df],target_file_path=os.path.join(target_dir,data_file_name))


def parse_features_to_df(audio_file_path, PTSS_label):
    smile = opensmile.Smile(
        feature_set=opensmile.FeatureSet.eGeMAPSv02,
        feature_level=opensmile.FeatureLevel.LowLevelDescriptors,
        logfile='smile.log'
        )   

    #create DataFrame with LLD values for each time increment
    df = smile.process_file(audio_file_path)#DataFrame

    #add label
    if(PTSS_label == True):
        ptss_num = 1
    else:
        ptss_num = 0

    df["PTSS-nr"] = ptss_num
    df["PTSS"] = PTSS_label
    
    return df

def save_raw_data(new_df_list,target_file_path="raw_data.json"):
    with open(target_file_path, 'a') as f:
        for df in new_df_list:
            f.write(df.to_json()+"\n")


if __name__ == "__main__":
    main()
