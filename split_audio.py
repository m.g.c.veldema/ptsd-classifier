from pydub import AudioSegment
import os

from tqdm import tqdm

target_dir = r"E:\thesis_audio\split-audio-2"
data_dir_path = r"E:\thesis_audio\sessie 4"

def main():
	split_files_from_directory(data_dir_path)

def split_files_from_directory(dir_path):
	for filename in tqdm(os.listdir(dir_path),colour="red"):
		if not filename.endswith(".wav"):
			continue
		f = os.path.join(dir_path, filename)
		#print("\n{}:".format(f))
		split_in_fragments(filename)


def split_in_fragments(filename):
	file_path = os.path.join(data_dir_path, filename)

	fragments_size = 2 #in seconds

	audio = AudioSegment.from_file(file_path,format='wav')
	length = audio.duration_seconds

	t0 = 0
	i = 0
	t2 = 0
	while t2 < length:
		#get indexes
		t1 = i * fragments_size
		t2 = (i+1) * fragments_size

		#corrent for end
		if t2 > length:
			t2 = length

		newAudio = audio[t1*1000:t2*1000] #Works in milliseconds

		#check for lenght of fragment smash last two together if needed
		if newAudio.duration_seconds < fragments_size/2:
			newAudio = audio[t0*1000:t2*1000]
			i -= 1
		t0 = t1

		i+=1

		new_file_path = os.path.join(target_dir,filename.replace('.wav',"-{}.wav".format(i)))

		newAudio.export(new_file_path, format="wav") #Exports to a wav file in the current path.

		#print(new_file_path)

		

if __name__ == "__main__":
	main()
