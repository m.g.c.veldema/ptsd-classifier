
-------------
test report:
-
score:	0.4669111720135418
-
              precision    recall  f1-score   support

    non-PTSD       0.60      0.46      0.52     15511
        PTSD       0.35      0.48      0.40      9301

    accuracy                           0.47     24812
   macro avg       0.47      0.47      0.46     24812
weighted avg       0.50      0.47      0.48     24812


-------------
train report:
-
score:	0.7613653450954839
-
              precision    recall  f1-score   support

    non-PTSD       0.73      0.86      0.79     10480
        PTSD       0.81      0.65      0.72      9471

    accuracy                           0.76     19951
   macro avg       0.77      0.76      0.76     19951
weighted avg       0.77      0.76      0.76     19951
