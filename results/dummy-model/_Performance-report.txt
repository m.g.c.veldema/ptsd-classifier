
-------------
test report:
-
score:	0.6394615247911853
-
              precision    recall  f1-score   support

    non-PTSD       0.64      1.00      0.78     14393
        PTSD       0.00      0.00      0.00      8115

    accuracy                           0.64     22508
   macro avg       0.32      0.50      0.39     22508
weighted avg       0.41      0.64      0.50     22508


-------------
train report:
-
score:	0.5211413165580768
-
              precision    recall  f1-score   support

    non-PTSD       0.52      1.00      0.69     11598
        PTSD       0.00      0.00      0.00     10657

    accuracy                           0.52     22255
   macro avg       0.26      0.50      0.34     22255
weighted avg       0.27      0.52      0.36     22255
