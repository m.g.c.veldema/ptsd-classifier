dtreeviz==1.3
pydub==0.25.1
pandas==1.3.0
opensmile==2.1.3
tqdm==4.61.2
numpy==1.21.0
scipy==1.7.0
scikit_learn==0.24.2