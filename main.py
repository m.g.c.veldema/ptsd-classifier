import pandas as pd
import numpy as np

from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.metrics import classification_report

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GroupKFold
from sklearn.model_selection import StratifiedShuffleSplit

from dtreeviz.trees import dtreeviz

import numpy
import os


from tqdm.auto import tqdm
import random

import os

data_file_path = "data.json"
data_folder_path = r"E:\thesis_audio\data-2\extended-data"#"E:\thesis_audio\data\derivative_data"

result_folder = "results/my-model"

if not os.path.exists(result_folder):
	os.makedirs(result_folder)

class_dict = {
		True : "PTSS",
		False: "non-PTSD"
	}

classes = ["non-PTSD", "PTSD"]
interview_ids = [202,626,641,645,663,696,816,847,896,1713,1714]
interview_ids.extend([316,338,683])

def main():

	## Load the data per interview and create the data set
	interviews = dict()
	for iid in tqdm(interview_ids,desc="loading interviews",colour="blue",position=0):
		interviews[iid] = interview(iid)

	full_data = data_set(list(interviews.values()),"Total")
	X = full_data.X
	Y = full_data.Y
	IDs = full_data.IDs
	Labels = full_data.labels
	

	#show_individual_features(full_data)

	## split the data in train, test and optionally validation set:
	ran_state = 80436#random.randint(0,100000)
	ran_val_state = 5424#random.randint(0,100000)

	train_set, test_set, val_set = split_data(interviews,False,ran_state,ran_val_state)

	## Find the best hyper paramters
	#best_max_features, best_max_depth, best_max_leaves = hyper_parameter_search2(train_set)

	## with the best hyper paramters create the model based on the train set
	clf = DecisionTreeClassifier(criterion="entropy",
					max_features= 1,#8,#best_max_features,
					max_depth=1,# best_max_depth,
					max_leaf_nodes= 2,#best_max_leaves
					)
	train(clf,train_set)


	## report performance
	train_report_string = form_report(clf,train_set)
	test_report_string = form_report(clf,test_set)

	with open("{}/_Performance-report.txt".format(result_folder),'w') as f:
		f.writelines([test_report_string+"\n", train_report_string])

	tree.plot_tree(clf,feature_names=Labels,class_names=classes,rounded=True,impurity=True)

	interview_scores(clf,train_set.interviews,"train data:")
	interview_scores(clf,test_set.interviews,"test data:")

	## visualize model
	show_tree(clf,train_set,title="Train Set")
	show_tree(clf,test_set,title="Test Set")
	show_tree(clf,Labels,X,Y,title="Totaal")

def interview_scores(clf,interview_list,title=None):
	"""
	Calculate and save the accuracy of the clf model for each interview in the interview list
	"""
	with open("{}/_interview-scores.txt".format(result_folder),'a') as f:
		if not title == None:
			f.write("------\n{}:\n".format(title))
			print("\n"+title)
		for interview_i in interview_list:
			label = "PTSD\t" if interview_i.y == True else "non-PTSD"

			res = "{label}\t{id}:\t{score}\n".format(
				label=label,
				id=interview_i.id,
				score=interview_i.model_accuracy(clf),
				)
			f.write(res)
			print(res,end="")

def show_individual_features(subset):
	"""
	Iterates over all features and visualises the distribution of the data in the subset for each of them.
	"""
	X = subset.X
	Y = subset.Y
	Labels = subset.labels

	clf = DecisionTreeClassifier(criterion="entropy",
					max_features=None,
					max_depth=1,
					max_leaf_nodes=None
					)
	for label in tqdm(range(92,len(Labels)),colour="blue",leave=True):
		if label >= len(Labels):
			break

		Xs = [[x[label]] for x in X]

		train(clf,Xs,Y)
		show_tree(clf,[Labels[label]],Xs,Y,title="{}. {}".format(label,Labels[label]))

def show_tree(clf,subset,title=None, show_path=False):
	"""
	Creates, saves and visualises a tree representation of the clf model with the datapoint distribution of the subset
	"""
	Labels = subset.labels
	X = subset.X
	Y = subset.Y

	viz = dtreeviz(clf,
		np.array(np.array(X)),
		numpy.array(Y),
		feature_names=Labels,
		class_names=classes,
		target_name="PTSD",
		show_node_labels=True,
		fancy=True,
		show_root_edge_labels=True,
		title=title,
		show_just_path=show_path,

	)
	filepath = "{}/{}-tree-model.svg".format(result_folder,str(title).replace(':','-').replace('.',' '))
	viz.save(filepath)
	print("tree viz saved to:",filepath)
	viz.view()


def split_data(interview_dict, validation=False, ran_state=None, ran_val_state=None):
	"""
	Split the data in a test and train set and optionally also a validation set from a dictionary of interview objects.
	The subsets are balanced per class.
	"""
	interview_nrs = list(interview_dict.keys())
	interview_labels = [x.y for x in interview_dict.values()]

	#is split statis fied/ balanced: https://scikit-learn.org/stable/modules/cross_validation.html#stratification

	sss = StratifiedShuffleSplit(n_splits=2, test_size=0.5,random_state=ran_state)

	respack = sss.split(X=interview_nrs,y=interview_labels)
	splitA, splitB = respack
	train_indices, test_indices = splitA
	train_ids =[interview_nrs[i] for i in train_indices]
	test_ids = [interview_nrs[i] for i in test_indices]

	#train_ids, test_ids, Y_train, Y_test = train_test_split(interview_nrs, interview_labels,random_state=ran_state,)

	val_ids = []
	if validation:
		train_ids, val_ids, Y_train, Y_val = train_test_split(train_ids,np.zeros(len(train_ids)), random_state=ran_val_state)

	train = [ interview_dict[id] for id in train_ids]
	test = 	[ interview_dict[id] for id in test_ids	]
	val = 	[ interview_dict[id] for id in val_ids	]

	data_set_sizes_path = "{}/_data-set-sizes.txt".format(result_folder)
	with open(data_set_sizes_path, 'a') as f:
		to_write = [
			"With random state=\t{}\n".format(ran_state),
			"With random val state=\t{}\n-\n".format(ran_val_state),
			"Data set split sizes:\n",
			"test:\t{} interviews\n".format(len(test)),
			"train:\t{} interviews\n".format(len(train)),
			"val:\t{} interviews\n".format(len(val)),	
		]
		f.writelines(to_write)

	return data_set(train,"train"), data_set(test,"test"), data_set(val,"validation")

def split_x_y(interview_list, set_log_name=None):
	""" 
	Merges the X matrices and the Y lists of the interviews to one feature matrix X and one class list Y list.
	"""
	X = []
	Y = []

	for interview_i in tqdm(interview_list,desc="merging X set",leave=False, colour="yellow"):
		X.extend(interview_i.X)
		Y.extend(interview_i.Y)

	if not set_log_name == None:
		with open("{}/_data-set-sizes.txt".format(result_folder), 'a') as f:
			f.write(
				"\n{}:\t{} datapoints".format(set_log_name,len(X))
			)
	return X, Y


def hyper_parameter_search(train_set):#with cross validation
	"""
	For each combination of hyper parameters, evaluates the performance using cross validation.
	The best hyper parameter set is returned as a 3-tuple.

	Returns: ( best_max_features, best_max_depth, best_max_leaves )
	"""
	X_train = train_set.X
	Y_train = train_set.Y
	train_IDs = train_set.IDs #groups

	id_kfold = GroupKFold(n_splits=train_set.interview_count)

	none_to_int_value = lambda j: j or 15

	best_accuracy = 0
	best_hp = 0,0,0
	best_total_hp = 0
	best_stdv = 0

	print("Hyper parameter search:")
	i = 0
	for vmax_features in tqdm(range(0,min(14,len(X_train[0]))),colour='red',desc='f',position=0):
		if vmax_features <=0:
			vmax_features = None
		for vmax_depth in tqdm(range(0,14),colour='green',desc='d',position=1,leave=False):
			if vmax_depth <= 0:
				vmax_depth = None
			for vmax_leaves in tqdm(range(1,14),colour='blue',desc='l',position=2,leave=False):
				if vmax_leaves <= 1:
					vmax_leaves = None
				
				i+=1

				temp_clf = DecisionTreeClassifier(criterion="entropy",
					max_depth=vmax_depth,
					max_leaf_nodes=vmax_leaves,
					max_features=vmax_features
					)
				total_hp = 0
				all_temp_res = ""
				total_accuracy = 0

				temp_scores = cross_val_score(temp_clf, X_train, Y_train,cv=id_kfold,groups=train_IDs)

				total_accuracy = sum(temp_scores)
				temp_mean = temp_scores.mean()
				temp_stdv = temp_scores.std()

				total_hp = sum([none_to_int_value(x) for x in [vmax_leaves,vmax_depth,vmax_features]])

				temp_hp_res = "--------\ni:{}\naccuracy:\t{}\n-\nSt. dev:\t{}\n-\nmax_features:\t{}\nmax_depth:\t{}\nmax_leaves:\t{}\nscores:".format(i,temp_mean,temp_stdv,vmax_features,vmax_depth,vmax_leaves)

				temp_score_string = ""
				
				for score in temp_scores:
					temp_score_string = "{}\n\t{}".format(temp_score_string,score)

				temp_res = temp_hp_res + temp_score_string
				with open("{}/hyper-parameter-validation.txt".format(result_folder),'a') as f:
					f.write(temp_res)
				

				if(total_accuracy > best_accuracy or (total_accuracy > best_accuracy - 0.5*best_stdv and total_hp < best_total_hp)):
					best_accuracy = total_accuracy
					best_hp = vmax_features, vmax_depth, vmax_leaves
					best_total_hp = total_hp
					best_stdv = temp_stdv
					best_res_string = temp_res
					#print(temp_res)
					with open("{}/_best-hyper-parameters.txt".format(result_folder), 'w') as g:
						g.write(best_res_string)
					

	best_max_features, best_max_depth, best_max_leaves = best_hp
	
	return best_max_features, best_max_depth, best_max_leaves

def train(clf, train_set):
	X_train = train_set.X
	Y_train = train_set.Y
	return clf.fit(X_train,Y_train)

def evaluate(clf,subset):
	X = subset.X
	Y = subset.Y
	return clf.score(X,Y)

def form_report(clf,subset):
	"""
	Evaluates the clf model's performance on the subset and returns a report as a string.

	More about classification report: https://scikit-learn.org/stable/modules/generated/sklearn.metrics.classification_report.html
	"""
	score = evaluate(clf,subset)

	print("{} score:\t{}".format(subset.name,score))

	report = str(classification_report(
		y_true = subset.Y,
		y_pred = clf.predict(subset.X),
		target_names=classes,
		zero_division=0,
	))

	res = "\n-------------\n{} report:\n-\nscore:\t{}\n-\n{}".format(subset.name,score,report)
	return res

def df_to_x_y(datapoint_df):
	"""
	Parses the PTSD label, feature list, and label list from a smile dataframe.
	"""
	y = datapoint_df.pop("PTSS")[0]
	y_nr = datapoint_df.pop("PTSS-nr")[0]

	labels = ["{}:{}".format(row,col) for col in datapoint_df.columns for row in datapoint_df.index]

	x = datapoint_df.to_numpy().transpose()

	x = x.flatten()

	### for dummy model ###
	# newlabels = ["dummy"]
	# newx = [0]

	# x = newx
	# labels = newlabels
	### for dummy model ###

	return x,y,y_nr,labels

def load_interview_data(interview_id):
	"""
	Loads and parses the feature matrix X, the class list Y, and feature label list labels for the interview of the given id.
	"""
	json_datapoints = []
	filename = "{}.json".format(str(interview_id))

	with open(os.path.join(data_folder_path,filename),'r') as f:
		json_datapoints.extend(f.readlines())

	datapoints_list = [pd.read_json(x) for x in tqdm(json_datapoints,desc="converting data from {}".format(filename),leave=False)]

	X,Y,Y_nr = [],[],[]

	for datapoint in tqdm(datapoints_list,desc="fetching datapoints from {}".format(filename),leave=False):
		x,y,y_nr,labels = df_to_x_y(datapoint)
		X.append(x)
		Y.append(y)
		Y_nr.append(y_nr)

	return X,Y,Y_nr,labels


### interview object class###
class interview:

	def __init__(self, id):
		self.id = id

		X,Y,Y_nr,labels = load_interview_data(str(id))

		self.X = X #indexed by datapoint
 
		self.XT = np.transpose(self.X) # indexed by feature

		self.y = Y[0]
		self.y_nr = Y_nr[0]
		self.Y = Y
		self.Y_nr = Y_nr

		self.labels = labels

		self.data_point_count = len(self.X)

		self.ID = [id] * self.data_point_count

	def model_accuracy(self,clf):
		"""
		Calculates and returns the accuracy of the clf model in this interview
		"""
		return clf.score(self.X,self.Y)


class data_set:
	def __init__(self, interview_list, name=None):
		self.interviews = interview_list

		self.name = name

		self.X, self.Y = split_x_y(interview_list,name)

		self.ids = [x.id for x in interview_list]
		self.set_IDs()

		self.data_point_count = len(self.X)
		self.interview_count = len(self.interviews)

		self.labels = []
		if len(interview_list) > 0:
			self.labels = self.interviews[0].labels
		
	def set_IDs(self):
		IDs = []

		for interview_i in self.interviews:
			IDs.extend(interview_i.ID)

		self.IDs = IDs


if __name__ == "__main__":
	main()