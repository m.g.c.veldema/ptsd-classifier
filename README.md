# PTSD classification
> Code for my bachelor's thesis in artificial intelligence at Utrecht University

Code to generate a PTSD/non-PTSD classifier for oral history. Used for interviews of Dutch UNIFIL veterans of https://veteranenvertellen.nl/ by paralinguistic analysis.

## Using this project

To use this project, clone it locally and install the packages in requirements.txt .

```shell
cd PATH/TO/WORKING/DIRECTORY
git clone https://git.science.uu.nl/m.g.c.veldema/ptsd-classifier.git
pip install -r requirements.txt
```

This will copy the project to you local drive and install the required packages.

### Initial Configuration

You need to download the interviews and save them as {interview_id}.wav. Make sure that their in .wav format.
Then change the source, target and data folder strings in the scripts you want to use to suit your situation. 

## Scripts

The scripts follow a certain order to be used:

1. split_audio.py: splits the audio files in "data_dir_path" in 2 second fragments and saves them to "target_dir";

2. store_data.py: processes the audio files in "source_dir" using opensmile and stores the raw data in "target dir";

3. clean_data.py: simplifies the raw data in "data_dir_path" by aggregating the max, min, mean, stander deviation and the median and storing it as a json representation of a pandas.Dataframe in "target_dir_path";

4. main.py: Splits the cleaned data in "data_file_path" in at least a train and test set, creates a model based on the train data and evaluates it using the test data. Results are saved to "result_folder" and model is visualised on the train and test set; and

0. f-test.py: For every interview in "ids", calculates a similarity percentage by doing an F-test on all features. Also list which features are in common in at leas one pair and which features are in common in all pairs.

Besides these, two models can be found in the results folder together with their performance reports.

## Links

