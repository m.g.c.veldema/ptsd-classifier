import pandas as pd
import os
from tqdm import tqdm

def main():
	data_dir_path = r"E:\thesis_audio\data-2\raw_data"
	target_dir_path = r"E:\thesis_audio\data-2\extended-data"
	for filename in tqdm(["316.json","338.json","683.json"],colour="green",position=0,desc='files'):
		
		file_path = os.path.join(data_dir_path, filename)

		f = open(file_path, 'r')
		lines = f.readlines()
		f.close()

		res_list = derivative_df(lines)
		with open(os.path.join(target_dir_path,filename),'a') as g:
			for res_df in res_list:
				g.write(res_df.to_json()+"\n")

def derivative_df(lines):
	result = []
	for line in tqdm(lines,colour="blue",leave=False,desc='file'):
		df = pd.read_json(line)
		PTSS_label = df.pop('PTSS')[0]
		PTSS_nr_label = df.pop('PTSS-nr')[0]

		df = df.agg(['max','min','mean','std','median'])

		df['PTSS'] = PTSS_label
		df['PTSS-nr'] = PTSS_nr_label

		result.append(df)

	return result

if __name__ == "__main__":
	main()