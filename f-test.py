import pandas as pd
import numpy as np
from tqdm.auto import tqdm
from scipy import stats

from main import data_set, interview

similar_set = set()
not_similar_set = set()
def main():
	ids = [316,338,645,683,896,1713,1714]
	nonPTSDids = [202,626,641,663,696,816,847]

	PTSD_interviews = [interview(id) for id in tqdm(ids)]
	PTSD_data_set = data_set(PTSD_interviews)

	the_labels = PTSD_interviews[0].labels

	f = open("_f-test-alpha-015.txt",'w')
	result = equality_test(PTSD_data_set,f,alpha=0.15)
	write_and_print(f,result)
	write_and_print(f)

	similar = [the_labels[x] for x in similar_set]
	similar_all = similar_set.difference(not_similar_set)
	similar_all_list = [the_labels[x] for x in similar_all]
	write_and_print(f,"similar features:")
	for s in similar:
		write_and_print(f,s)
	
	write_and_print(f)
	write_and_print(f,"common similarities:")
	for s in similar_all_list:
		write_and_print(f,s)
	
	f.close()

	

def write_and_print(f,string=""):
	f.write("\n"+str(string))
	print(string)


def equality_test(data_set,f, alpha=0.05):
	write_and_print(f,"alpha:\t{}".format(alpha))
	ids = [x.id for x in data_set.interviews]
	percantage_df = pd.DataFrame(columns=ids,index=ids)
	for interview_i in data_set.interviews:
		res = [equality_percentage(interview_i,interview_j,alpha) for interview_j in data_set.interviews]
		percantage_df[interview_i.id] = res
	
	return percantage_df


def equality_percentage(i,j, alpha=0.05):
	# if(i.id == j.id):
	# 	return 100
	t = 0
	res_list = []
	for k in range(max(len(i.XT), len(j.XT))):
		x = i.XT[k]
		y = j.XT[k]

		if k == 44:
			k+=0

		if x.min == y.min and x.max == y.max and x.min == x.max:
			res = True
		else:
			res = is_equal_var(x,y,alpha)
		res_list.append((res,x,y))
		if res:
			t += 1
			similar_set.add(k)
		else:
			not_similar_set.add(k)

	proportion = t/max(len(i.XT), len(j.XT)) * 100
	return proportion

def is_equal_var(a,b, alpha=.05):
	f, p = f_test(a,b)
	#f, p = stats.levene(a,b)
	res = p > alpha
	if np.isnan(p) or np.isnan(f):
		res =  True
	return res

def f_test(a,b, print_on=False,k=None):
	"""
	taken from: https://www.statology.org/f-test-python/ on august 6th 2021
	"""
	a = np.array(a)
	b = np.array(b)

	a_var = np.var(a,ddof=1)
	b_var =  np.var(b,ddof=1)

	#variance of a needs to be larger
	if b_var > a_var:
		a,b = b,a
		a_var,b_var = b_var, a_var

	f= a_var/ b_var

	#degrees of freedom
	df_a = len(a) - 1
	df_b = len(b) - 1

	p = 1 - stats.f.cdf(f,df_a,df_b) #p-waarde vinden

	return f, p


if __name__ == "__main__":
	main()