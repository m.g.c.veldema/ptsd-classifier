
alpha:	0.05
       316    338    645    683    896    1713   1714
316   100.0   24.0   11.2    9.6   16.0    8.0    5.6
338    24.0  100.0   17.6   10.4   16.0    4.8    6.4
645    11.2   17.6  100.0   19.2   13.6   16.0   10.4
683     9.6   10.4   19.2  100.0   13.6    8.8   12.0
896    16.0   16.0   13.6   13.6  100.0    6.4   12.8
1713    8.0    4.8   16.0    8.8    6.4  100.0   10.4
1714    5.6    6.4   10.4   12.0   12.8   10.4  100.0

similar features:
max:Loudness_sma3
min:Loudness_sma3
mean:Loudness_sma3
std:Loudness_sma3
median:Loudness_sma3
max:alphaRatio_sma3
min:alphaRatio_sma3
mean:alphaRatio_sma3
std:alphaRatio_sma3
median:alphaRatio_sma3
max:hammarbergIndex_sma3
min:hammarbergIndex_sma3
mean:hammarbergIndex_sma3
std:hammarbergIndex_sma3
median:hammarbergIndex_sma3
max:slope0-500_sma3
min:slope0-500_sma3
mean:slope0-500_sma3
std:slope0-500_sma3
median:slope0-500_sma3
max:slope500-1500_sma3
min:slope500-1500_sma3
mean:slope500-1500_sma3
std:slope500-1500_sma3
median:slope500-1500_sma3
max:spectralFlux_sma3
min:spectralFlux_sma3
mean:spectralFlux_sma3
std:spectralFlux_sma3
median:spectralFlux_sma3
max:mfcc1_sma3
min:mfcc1_sma3
mean:mfcc1_sma3
std:mfcc1_sma3
median:mfcc1_sma3
max:mfcc2_sma3
min:mfcc2_sma3
mean:mfcc2_sma3
std:mfcc2_sma3
median:mfcc2_sma3
max:mfcc3_sma3
min:mfcc3_sma3
mean:mfcc3_sma3
std:mfcc3_sma3
median:mfcc3_sma3
max:mfcc4_sma3
min:mfcc4_sma3
mean:mfcc4_sma3
std:mfcc4_sma3
median:mfcc4_sma3
max:F0semitoneFrom27.5Hz_sma3nz
min:F0semitoneFrom27.5Hz_sma3nz
mean:F0semitoneFrom27.5Hz_sma3nz
std:F0semitoneFrom27.5Hz_sma3nz
median:F0semitoneFrom27.5Hz_sma3nz
max:jitterLocal_sma3nz
min:jitterLocal_sma3nz
mean:jitterLocal_sma3nz
std:jitterLocal_sma3nz
median:jitterLocal_sma3nz
max:shimmerLocaldB_sma3nz
min:shimmerLocaldB_sma3nz
mean:shimmerLocaldB_sma3nz
std:shimmerLocaldB_sma3nz
median:shimmerLocaldB_sma3nz
max:HNRdBACF_sma3nz
min:HNRdBACF_sma3nz
mean:HNRdBACF_sma3nz
std:HNRdBACF_sma3nz
median:HNRdBACF_sma3nz
max:logRelF0-H1-H2_sma3nz
min:logRelF0-H1-H2_sma3nz
mean:logRelF0-H1-H2_sma3nz
std:logRelF0-H1-H2_sma3nz
median:logRelF0-H1-H2_sma3nz
max:logRelF0-H1-A3_sma3nz
min:logRelF0-H1-A3_sma3nz
mean:logRelF0-H1-A3_sma3nz
std:logRelF0-H1-A3_sma3nz
median:logRelF0-H1-A3_sma3nz
max:F1frequency_sma3nz
min:F1frequency_sma3nz
mean:F1frequency_sma3nz
std:F1frequency_sma3nz
median:F1frequency_sma3nz
max:F1bandwidth_sma3nz
min:F1bandwidth_sma3nz
mean:F1bandwidth_sma3nz
std:F1bandwidth_sma3nz
median:F1bandwidth_sma3nz
max:F1amplitudeLogRelF0_sma3nz
min:F1amplitudeLogRelF0_sma3nz
mean:F1amplitudeLogRelF0_sma3nz
std:F1amplitudeLogRelF0_sma3nz
median:F1amplitudeLogRelF0_sma3nz
max:F2frequency_sma3nz
min:F2frequency_sma3nz
mean:F2frequency_sma3nz
std:F2frequency_sma3nz
median:F2frequency_sma3nz
max:F2bandwidth_sma3nz
min:F2bandwidth_sma3nz
mean:F2bandwidth_sma3nz
std:F2bandwidth_sma3nz
median:F2bandwidth_sma3nz
max:F2amplitudeLogRelF0_sma3nz
min:F2amplitudeLogRelF0_sma3nz
mean:F2amplitudeLogRelF0_sma3nz
std:F2amplitudeLogRelF0_sma3nz
median:F2amplitudeLogRelF0_sma3nz
max:F3frequency_sma3nz
min:F3frequency_sma3nz
mean:F3frequency_sma3nz
std:F3frequency_sma3nz
median:F3frequency_sma3nz
max:F3bandwidth_sma3nz
min:F3bandwidth_sma3nz
mean:F3bandwidth_sma3nz
std:F3bandwidth_sma3nz
median:F3bandwidth_sma3nz
max:F3amplitudeLogRelF0_sma3nz
min:F3amplitudeLogRelF0_sma3nz
mean:F3amplitudeLogRelF0_sma3nz
std:F3amplitudeLogRelF0_sma3nz
median:F3amplitudeLogRelF0_sma3nz

common similarities:
min:jitterLocal_sma3nz